var start = new Date().getTime();
var result;
var unit = "ms";

//code here

// structure = {nbr: 0, factSum: 0, amicable: false}
var factorSum = [], abNbr = [], current = {};
for (var i = 1; i < 28123; i++) {
    current = {};
    current = {nbr: i, factSum: 0, abundant: false};
    for(var j = 1; j < i; j++) {
        var div = i / j;
        //si pas de chiffre après la virgule : facteur
        if(div - parseInt(div) === 0) {
            current.factSum += j;
            // console.log(div);
        }
    }
    if(current.factSum > current.nbr) {
        current.abundant = true
        abNbr.push(i);
    }
    //factorSum.push(current);
    // console.log(current);
}
console.log('factsums ok');
var sumable = new Set(), sum;

abNbr.forEach(termOne => {
    abNbr.forEach(termTwo => {
        sum = termOne + termTwo;
        sumable.add(sum)
    })
});
console.log('sumables ok : ' + sumable.length + ' items');
result = 0;
for (var i = 1; i < 28123; i++) {
    if(!sumable.has(i)) {
        result += i;
    }
}

var duration = new Date().getTime() - start;
if(duration > 60000) {
    duration = (duration/1000)/60;
    unit = "min";
    console.log("Algo a revoir :/")
} else if(duration > 60) {
    unit = "sec";
    duration = duration/1000
}
console.log("resultat : " + result + " | Duration : " + duration + " " + unit);
