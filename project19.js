var start = new Date().getTime();
var result;
var unit = "ms";
// import { MyDate } from './mylib'
class MyDate {

    constructor(
        year,
        month,
        day,
        dayOfWeek

    ) {
        this.year = year ? year : 1900;
        this.month = month ? month : 1;
        this.day = day ? day : 1;
        this.dayOfWeek = this.calcDayOfWeek();
        // this.weeks = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi']
        this.weeks = ['samedi', 'dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi']
    }

    /**
     * add one day to date
     */

    addDay() {
        this.day++;
        this.dayOfWeek++;
        this.dayOfWeek = this.dayOfWeek%7;
        // console.log("calc vs inc : " + this.calcDayOfWeek() + ' <-> ' + this.dayOfWeek);
        if(this.day > this.lastDayOfMonth()) {
            this.day = 1;
            this.addMonth()
        }
    }

    addMonth() {
        this.month++;
        if(this.month > 12) {
            this.month = 1;
            this.addYear();
        }
    }

    addYear() {
        this.year++;
    }

    lastDayOfMonth() {
        var lastDay;
        switch(this.month) {
            case 4:
            case 6:
            case 9:
            case 11:
                lastDay = 30;
                break;
            case 2:
                lastDay = 28;
                if(this.isYearBisextile()) {
                    lastDay = 29;
                }
                break;
            default:
                lastDay = 31;
        }
        return lastDay;
    }

    isEqual(date) {
        return this.year === date.getYear() && this.month === date.getMonth() && this.day === date.getDay()
    }

    isBefore(date) {
        if(this.year < date.getYear())
            return true;
        else if(this.year > date.getYear())
            return false;
        if(this.month < date.getMonth())
            return true;
        else if(this.month > date.getMonth())
            return false;
        if(this.day < date.getDay())
            return true;
        else if(this.day > date.getDay())
            return false;
    }

    isAfter(date) {
        if(this.year > date.getYear())
            return true;
        else if(this.year < date.getYear())
            return false;
        if(this.month > date.getMonth())
            return true;
        else if(this.month < date.getMonth())
            return false;
        if(this.day > date.getDay())
            return true;
        else if(this.day < date.getDay())
            return false;
    }

    calcDayOfWeek() {
        var monthTable = [0,3,3,6,1,4,6,2,5,0,3,5];
        if(this.isYearBisextile()) {
            monthTable[0] = 6;
            monthTable[1] = 2;
        }

        var yearsTable = [];
        yearsTable.push([0,6,17,23,28,34,45,51,56,62,73,79,84,90]);
        yearsTable.push([1,7,12,18,29,35,40,46,57,63,68,74,85,91,96]);
        yearsTable.push([2,13,19,24,30,41,47,52,58,69,75,80,86,97]);
        yearsTable.push([3,8,14,25,31,36,42,53,59,64,70,81,87,92,98]);
        yearsTable.push([9,15,20,26,37,43,48,54,65,71,76,82,93,99]);
        yearsTable.push([4,10,21,27,32,38,49,55,60,66,77,83,88,94]);
        yearsTable.push([5,11,16,22,33,39,44,50,61,67,72,78,89,95]);

        var centuriesTable = [];
        centuriesTable.push([5, 12, 20, 24]);
        centuriesTable.push([11, 18, 19, 23]);
        centuriesTable.push([17, 10]);
        centuriesTable.push([18, 22, 16, 9]);
        centuriesTable.push([15, 8]);
        centuriesTable.push([17, 21, 14, 7]);
        centuriesTable.push([13, 6]);

        var year = this.year%100;
        var century = parseInt(this.year/100);
        var d = this.day;
        var m = monthTable[this.month-1];
        var y;
        for(var i in yearsTable) {
            if(yearsTable[i].indexOf(year) > -1) {
                y = parseInt(i);
                break;
            }
        }
        var c;
        for(var i in centuriesTable) {
            if(centuriesTable[i].indexOf(century) > -1) {
                c = parseInt(i);
                break;
            }
        }
        var y4 = parseInt(year/4);
        var res = (d+m+year+y4+c)%7;

        return res;
    }

    isYearBisextile() {
        return this.year % 1000 === 0 && this.year % 400 === 0 || this.year % 4 === 0;
    }

    getYear() {
        return this.year;
    }
    getMonth() {
        return this.month;
    }
    getDay() {
        return this.day;
    }
    getDayOfWeek() {
        return this.dayOfWeek;
    }
    setYear(year) {
        this.year = year;
        return this.year;
    }
    setMonth(month) {
        this.month = month;
        return this.month;
    }
    setDay(day) {
        this.day = day;
        return this.day;
    }
    setDayOfWeek(dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        return this.dayOfWeek;
    }

    toString() {
        var str = "";
        str += this.weeks[this.dayOfWeek] + ' ';
        if(this.day < 10)
            str += "0";
        str += this.day + '/';

        if(this.month < 10)
            str += "0";
        str += this.month + '/' + this.year;
        return str;
    }
}
//code here
var date = new MyDate(1901, 1, 1);
// var date = new MyDate(2000, 1, 1);
    var months = [{
            "name": "Janvier",
            "duration": 31
        },
        {
            "name": "Février",
            "duration": 28
        },
        {
            "name": "Mars",
            "duration": 31
        },
        {
            "name": "Avril",
            "duration": 30
        },
        {
            "name": "Mai",
            "duration": 31
        },
        {
            "name": "Juin",
            "duration": 30
        },
        {
            "name": "Juillet",
            "duration": 31
        },
        {
            "name": "Août",
            "duration": 31
        },
        {
            "name": "Septembre",
            "duration": 30
        },
        {
            "name": "Octobre",
            "duration": 31
        },
        {
            "name": "Novembre",
            "duration": 30
        },
        {
            "name": "Décembre",
            "duration": 31
        }];

result = 0;
var last;
const target = new MyDate(2000, 12, 31);
while(date.isBefore(target)) {
    if(date.isYearBisextile()) {
        if(last !== date.getYear()) {
            last = date.getYear();
            // console.log('----------------- ' + date.getYear() + " est bisextile -----------------");
        }
    }
    // console.log(date.toString());
    if(date.getDay() === 1 && date.getDayOfWeek() === 1) {
        result++;
    }
    date.addDay();

}

var duration = new Date().getTime() - start;
if(duration > 60000) {
    duration = (duration/1000)/60;
    unit = "min";
    console.log("Algo a revoir :/")
} else if(duration > 60) {
    unit = "sec";
    duration = duration/1000
}
console.log("resultat : " + result + " | Duration : " + duration + " " + unit);
