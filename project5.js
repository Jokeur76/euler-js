var start = new Date().getTime();
var result;
var continu = true;
var i = 1;
while(continu) {
    if(isCorrect(i)) {
        continu = false;
        result = i;
    }
    i++;
}

function isCorrect(nbr) {
    for(var i = 1; i <= 20; i++) {
        if(nbr%i != 0) {
            return false;
        }
    }
    return true;
}

var duration = new Date().getTime() - start;
console.log("resultat : " + result + ". Duration : " + duration);