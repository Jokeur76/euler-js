var start = new Date().getTime();
var result = null;
var unit = "ms";
var primeFactor = [2];
var factors = [];

//notes
/*pour connaitre le nombre de diviseurs il faut multiplier les (puissances+1) du produits des facteurs premiers
ex: 840 = 2^3 * 3 * 5 * 7, soit 840 = 2^3 * 3^1 * 5^1 * 7^1
donc nbrDivisor = (3+1) * (1+1) * (1+1) * (1+1) = 32
 http://villemin.gerard.free.fr/Referenc/Prof/APROF/DivQte.htm
*/

//code here
var triangleNbr = 0;
var increment = 1;
var divisorsNbr;
while(result == null) {
    triangleNbr += increment;
    divisorsNbr = 0;
    calcNbdOfDivisor(triangleNbr)
    increment++;
}

var duration = new Date().getTime() - start;
if(duration > 60000) {
    duration = (duration/1000)/60;
    unit = "min"
    console.log("Algo a revoir :/")
} else if(duration > 60) {
    unit = "sec";
    duration = duration/1000
}
console.log("resultat : " + result + ". Duration : " + duration + " " + unit);

function calcNbdOfDivisor(triangle) {
    getPrimeFactors(triangle)
    divisorsNbr = 1;
    for(var i = 0;  i < factors.length; i++) {
        divisorsNbr *= factors[i]+1
    }
    if(divisorsNbr > 500) {
        result = triangle;
    }
}

function getPrimeFactors(nbr) {
    //exemple
    //840 2
    //420 2
    //210 2
    //105 3
    //35  5
    //7   7
    //1
    //840 = 2^3 * 3 * 5 * 7
    factors = [];
    var reste = nbr;
    //current factor
    var cf = 0;
    while(reste > 1) {
        if(primeFactor[cf] == undefined) pushNewPrimeFactor()
        if(factors[cf] == undefined) factors[cf] = 0;
        if(reste%primeFactor[cf] == 0) {
            reste = reste/primeFactor[cf];
            factors[cf] += 1;
        } else {
            cf++;
        }
    }
}

function pushNewPrimeFactor() {
    var testNbr = primeFactor[primeFactor.length - 1]+1;
    while(true) {
        if(isPrime(testNbr)) {
            primeFactor.push(testNbr);
            break;
        }
        testNbr++;
    }
}

function isPrime(nbr) {
    for(var prime of primeFactor) {
        if(nbr%prime == 0) return false
    }
    if(nbr = 1) return true
    for(var i = 2; i < nbr; i++) {
        if(nbr%i == 0) return false
    }
    return true;
}