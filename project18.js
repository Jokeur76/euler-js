var start = new Date().getTime();
var result = 0;
var unit = "ms";

//code here
var datasTest = [],
    datas = [];

datas.push([75]);
datas.push([95,64]);
datas.push([17,47,82]);
datas.push([18,35,87,10]);
datas.push([20,4,82,47,65]);
datas.push([19,1,23,75,3,34]);
datas.push([88,2,77,73,7,63,67]);
datas.push([99,65,4,28,6,16,70,92]);
datas.push([41,41,26,56,83,40,80,70,33]);
datas.push([41,48,72,33,47,32,37,16,94,29]);
datas.push([53,71,44,65,25,43,91,52,97,51,14]);
datas.push([70,11,33,28,77,73,17,78,39,68,17,57]);
datas.push([91,71,52,38,17,14,91,43,58,50,27,29,48]);
datas.push([63,66,4,68,89,53,67,30,73,16,69,87,40,31]);
datas.push([4,62,98,27,23,9,70,98,73,93,38,53,60,4,23]);

datasTest.push([3]);
datasTest.push([7,4]);
datasTest.push([2,4,6]);
datasTest.push([8,5,9,3]);
datasTest.push([7,2,9,8,5]);

// var batch = datasTest;
var batch = datas;
var cur = [0],
    max = 0,
    goOn = true,
    last = false,
    route = 0,
    loop = 0,
    dernier = [];

for(var i = 0; i < batch.length; i++) {
    cur[i] = 0;
}
while(goOn) {
    loop++;
    //reset route
    route = 0;
    //calc route
    for (var i = 0; i < cur.length; i++) {
        route += batch[i][cur[i]];
    }
    //set max
    if(route > max) max = route;

    //gere compteur en partant de la fin
    //j'incremente le dernier curseur
    for (var i = cur.length - 1; i >= 0; i--) {
        if(i === 0) goOn = false;
        if(cur[i] + 1 - cur[i-1] <= 1) {
            var log = 'B';
            dernier = JSON.parse(JSON.stringify(cur));
            cur[i]++;
            if(cur[i] > 0 && i !== cur.length -1) {
                log += '\'';
                for(var j = i; j < cur.length; j++) {
                    cur[j] = cur[i];
                }
            }
            break;
        }
    }
}


result = max;

var duration = new Date().getTime() - start;
if(duration > 60000) {
    duration = (duration/1000)/60;
    unit = "min";
    console.log("Algo a revoir :/")
} else if(duration > 60) {
    unit = "sec";
    duration = duration/1000
}
console.log("resultat : " + result + " | Duration : " + duration + " " + unit);

function derDif(curseur, dernier, index, delta) {
    var cur = JSON.parse(JSON.stringify(curseur));
    var der = JSON.parse(JSON.stringify(dernier));
    cur[index] += delta;
    if(dernier === undefined) {
        // console.log(true);
        return true;
    }
    for(var i = cur.length - 1; i >= 0; i--) {
        if(cur[i] !== der[i]) {
            // console.log(true);
            return true;
        }
    }
    // console.log(false);
    return false;
}