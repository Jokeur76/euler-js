var start = new Date().getTime();
var data = 600851475143;
var result;
//le plus grand facteur est contenu dans la racine carré
for(var i = Math.round(Math.sqrt(data)); i > 0; i--) {
    if(data%i == 0 && isPrime(i)) {
        result = i;
        break;
    }
}

function isPrime(nbr) {
    for(var i = 2; i < nbr; i++) {
        if(nbr%i == 0) return false;
    }
    return true;
}

var duration = new Date().getTime() - start;
console.log("resultat : " + result + ". Duration : " + duration);