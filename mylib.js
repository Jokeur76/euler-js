/**
 * Created by mickael on 15/06/17.
 */
export function isPrime(nbr) {
    for(var i = 2; i < nbr; i++) {
        if(nbr%i == 0) return false;
    }
    return true;
}

export function isPalindrome(data) {
    stringData = data.toString();
    stringDataReverse = stringData.split("").reverse().join("");
    //console.log("data : " + stringData + ", data reverse : " + stringDataReverse);

    return stringData == stringDataReverse;
}

export function stringPowa(nbr, powa) {
    var response = nbr;
    for(var i = 1; i < powa; i++) {
        response = stringProduct(response,nbr);
    }
    return response
}

export function stringProduct(nbr1, nbr2) {
    if(typeof nbr1 != "string") nbr1 = nbr1.toString()
    if(typeof nbr2 != "string") nbr2 = nbr2.toString()
    var longger, lower, product, temp,
        keep = "0",
        total = "";
    if(nbr1.length > nbr2.length) {
        longger = nbr1;
        lower = nbr2;
    } else {
        longger = nbr2;
        lower = nbr1;
    }

    for(var j = longger.length - 1; j >= 0; j--) {
        if(keep.length == 0) keep = "0";
        product = (parseInt(longger[j]) * parseInt(lower) + parseInt(keep.slice(keep.length - 1))).toString();
        keep = keep.slice(0,keep.length - 1);
        if(product.length > 1) {
            total = product.slice(product.length - 1) + total;
            product = product.slice(0,product.length - 1);
            temp = [];
            while(product.length > 0) {
                temp.unshift(parseInt(product.slice(product.length - 1)) + (keep.length > 0 ? parseInt(keep.slice(keep.length - 1)) : 0));
                product = product.slice(0,product.length - 1);
                keep= keep.slice(0,keep.length - 1)
            }
            keep = temp;
        } else {
            total = product + total;
        }
    }
    if(keep.length > 0) {
        var nbr;
        while(keep.length > 0) {
            if(keep.length > 1) {
                nbr = keep.pop().toString();
                total = nbr.slice(nbr.length - 1) + total;
                nbr = nbr.slice(0, nbr.length - 1)
                temp = [];
                for (var i = keep.length - 1; i >= 0; i--) {
                    temp.unshift(parseInt(keep[i]) + (nbr.length > 0 ? parseInt(nbr.slice(nbr.length - 1)) : 0));
                    nbr = nbr.slice(0, nbr.length - 1);
                }
                keep = temp;
            } else {
                total = keep.pop() + total;
            }
        }

    }
    return total;
}
export function sumOf(nbr1, nbr2) {
    if(typeof nbr1 != "string") nbr1 = nbr1.toString()
    if(typeof nbr2 != "string") nbr2 = nbr2.toString()
    var longger, lower, diff, sum,
        keep = 0,
        total = "";
    if(nbr1.length > nbr2.length) {
        longger = nbr1;
        lower = nbr2;
    } else {
        longger = nbr2;
        lower = nbr1;

    }
    diff = longger.length - lower.length;
    for(var j = longger.length - 1; j >= 0; j--) {
        sum = (parseInt(j-diff < 0 ? '0' : lower[j-diff]) + parseInt(longger[j]) + keep).toString();
        keep = 0;
        if(sum.length > 1) {
            total = sum.substr(1) + total;
            keep = parseInt(sum.substr(0,1));
        } else {
            total = sum + total;
        }
        if(sum.length > 2) throw new Error("ERROR ERROR ERROR")
    }
    if(keep > 0)
        total = keep + total;
    return total;
}
class MyDate {

    constructor(
        year,
        month,
        day,
        dayOfWeek

    ) {
        this.year = year ? year : 1900;
        this.month = month ? month : 1;
        this.day = day ? day : 1;
        this.dayOfWeek = this.calcDayOfWeek();
        // this.weeks = ['dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi', 'samedi']
        this.weeks = ['samedi', 'dimanche', 'lundi', 'mardi', 'mercredi', 'jeudi', 'vendredi']
    }

    /**
     * add one day to date
     */

    addDay() {
        this.day++;
        this.dayOfWeek++;
        this.dayOfWeek = this.dayOfWeek%7;
        // console.log("calc vs inc : " + this.calcDayOfWeek() + ' <-> ' + this.dayOfWeek);
        if(this.day > this.lastDayOfMonth()) {
            this.day = 1;
            this.addMonth()
        }
    }

    addMonth() {
        this.month++;
        if(this.month > 12) {
            this.month = 1;
            this.addYear();
        }
    }

    addYear() {
        this.year++;
    }

    lastDayOfMonth() {
        var lastDay;
        switch(this.month) {
            case 4:
            case 6:
            case 9:
            case 11:
                lastDay = 30;
                break;
            case 2:
                lastDay = 28;
                if(this.isYearBisextile()) {
                    lastDay = 29;
                }
                break;
            default:
                lastDay = 31;
        }
        return lastDay;
    }

    isEqual(date) {
        return this.year === date.getYear() && this.month === date.getMonth() && this.day === date.getDay()
    }

    isBefore(date) {
        if(this.year < date.getYear())
            return true;
        else if(this.year > date.getYear())
            return false;
        if(this.month < date.getMonth())
            return true;
        else if(this.month > date.getMonth())
            return false;
        if(this.day < date.getDay())
            return true;
        else if(this.day > date.getDay())
            return false;
    }

    isAfter(date) {
        if(this.year > date.getYear())
            return true;
        else if(this.year < date.getYear())
            return false;
        if(this.month > date.getMonth())
            return true;
        else if(this.month < date.getMonth())
            return false;
        if(this.day > date.getDay())
            return true;
        else if(this.day < date.getDay())
            return false;
    }

    calcDayOfWeek() {
        var monthTable = [0,3,3,6,1,4,6,2,5,0,3,5];
        if(this.isYearBisextile()) {
            monthTable[0] = 6;
            monthTable[1] = 2;
        }

        var yearsTable = [];
        yearsTable.push([0,6,17,23,28,34,45,51,56,62,73,79,84,90]);
        yearsTable.push([1,7,12,18,29,35,40,46,57,63,68,74,85,91,96]);
        yearsTable.push([2,13,19,24,30,41,47,52,58,69,75,80,86,97]);
        yearsTable.push([3,8,14,25,31,36,42,53,59,64,70,81,87,92,98]);
        yearsTable.push([9,15,20,26,37,43,48,54,65,71,76,82,93,99]);
        yearsTable.push([4,10,21,27,32,38,49,55,60,66,77,83,88,94]);
        yearsTable.push([5,11,16,22,33,39,44,50,61,67,72,78,89,95]);

        var centuriesTable = [];
        centuriesTable.push([5, 12, 20, 24]);
        centuriesTable.push([11, 18, 19, 23]);
        centuriesTable.push([17, 10]);
        centuriesTable.push([18, 22, 16, 9]);
        centuriesTable.push([15, 8]);
        centuriesTable.push([17, 21, 14, 7]);
        centuriesTable.push([13, 6]);

        var year = this.year%100;
        var century = parseInt(this.year/100);
        var d = this.day;
        var m = monthTable[this.month-1];
        var y;
        for(var i in yearsTable) {
            if(yearsTable[i].indexOf(year) > -1) {
                y = parseInt(i);
                break;
            }
        }
        var c;
        for(var i in centuriesTable) {
            if(centuriesTable[i].indexOf(century) > -1) {
                c = parseInt(i);
                break;
            }
        }
        var y4 = parseInt(year/4);
        var res = (d+m+year+y4+c)%7;

        return res;
    }

    isYearBisextile() {
        return this.year % 1000 === 0 && this.year % 400 === 0 || this.year % 4 === 0;
    }

    getYear() {
        return this.year;
    }
    getMonth() {
        return this.month;
    }
    getDay() {
        return this.day;
    }
    getDayOfWeek() {
        return this.dayOfWeek;
    }
    setYear(year) {
        this.year = year;
        return this.year;
    }
    setMonth(month) {
        this.month = month;
        return this.month;
    }
    setDay(day) {
        this.day = day;
        return this.day;
    }
    setDayOfWeek(dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        return this.dayOfWeek;
    }

    toString() {
        var str = "";
        str += this.weeks[this.dayOfWeek] + ' ';
        if(this.day < 10)
            str += "0";
        str += this.day + '/';

        if(this.month < 10)
            str += "0";
        str += this.month + '/' + this.year;
        return str;
    }
}