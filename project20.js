var start = new Date().getTime();
var result;
var unit = "ms";

//code here
var test = 100;
var factResult = stringFactorial(test);
var stringResult = factResult;
result = 0;
for(var i = 0; i < stringResult.length; i++) {
    result += parseInt(stringResult[i]);
}

var duration = new Date().getTime() - start;
if(duration > 60000) {
    duration = (duration/1000)/60;
    unit = "min";
    console.log("Algo a revoir :/")
} else if(duration > 60) {
    unit = "sec";
    duration = duration/1000
}
console.log("resultat : " + result + " | Duration : " + duration + " " + unit);

/**
 * need stringProduct method
 * @param nbr
 * @returns {number}
 */
function stringFactorial(nbr) {
    var res = nbr.toString();
    var fact = --nbr;
    while(fact > 0) {
        res = stringProduct(fact.toString(), res);
        fact--;
    }
    return res;
}

function stringProduct(nbr1, nbr2) {
    if(typeof nbr1 != "string") nbr1 = nbr1.toString()
    if(typeof nbr2 != "string") nbr2 = nbr2.toString()
    var longger, lower, product, temp,
        keep = "0",
        total = "";
    if(nbr1.length > nbr2.length) {
        longger = nbr1;
        lower = nbr2;
    } else {
        longger = nbr2;
        lower = nbr1;
    }

    for(var j = longger.length - 1; j >= 0; j--) {
        if(keep.length == 0) keep = "0";
        product = (parseInt(longger[j]) * parseInt(lower) + parseInt(keep.slice(keep.length - 1))).toString();
        keep = keep.slice(0,keep.length - 1);
        if(product.length > 1) {
            total = product.slice(product.length - 1) + total;
            product = product.slice(0,product.length - 1);
            temp = [];
            while(product.length > 0) {
                temp.unshift(parseInt(product.slice(product.length - 1)) + (keep.length > 0 ? parseInt(keep.slice(keep.length - 1)) : 0));
                product = product.slice(0,product.length - 1);
                keep= keep.slice(0,keep.length - 1)
            }
            keep = temp;
        } else {
            total = product + total;
        }
    }
    if(keep.length > 0) {
        var nbr;
        while(keep.length > 0) {
            if(keep.length > 1) {
                nbr = keep.pop().toString();
                total = nbr.slice(nbr.length - 1) + total;
                nbr = nbr.slice(0, nbr.length - 1)
                temp = [];
                for (var i = keep.length - 1; i >= 0; i--) {
                    temp.unshift(parseInt(keep[i]) + (nbr.length > 0 ? parseInt(nbr.slice(nbr.length - 1)) : 0));
                    nbr = nbr.slice(0, nbr.length - 1);
                }
                keep = temp;
            } else {
                total = keep.pop() + total;
            }
        }

    }
    return total;
}