var test = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
var test2 = [1,7,12,18,1,2,13,19,24,2,3,8,14,25,3,9,15,20,26,4,10,21,27,5,5,11,16,22,6,17,23,0];
var test3 = (1, 2, 3, 0)

var day = 1
var month = 6
var century = 19
var year = 52

function calcDayOfWeek() {
    var monthTable = [0,3,3,6,1,4,6,2,5,0,3,5];
    if(isYearBisextile()) {
        monthTable[0] = 6;
        monthTable[1] = 2;
    }

    var yearsTable = [];
    yearsTable.push([0,6,17,23,28,34,45,51,56,62,73,79,84,90]);
    yearsTable.push([1,7,12,18,29,35,40,46,57,63,68,74,85,91,96]);
    yearsTable.push([2,13,19,24,30,41,47,52,58,69,75,80,86,97]);
    yearsTable.push([3,8,14,25,31,36,42,53,59,64,70,81,87,92,98]);
    yearsTable.push([9,15,20,26,37,43,48,54,65,71,76,82,93,99]);
    yearsTable.push([4,10,21,27,32,38,49,55,60,66,77,83,88,94]);
    yearsTable.push([5,11,16,22,33,39,44,50,61,67,72,78,89,95]);

    var centuriesTable = [];
    centuriesTable.push([5, 12, 20, 24]);
    centuriesTable.push([11, 18, 19, 23]);
    centuriesTable.push([17, 10]);
    centuriesTable.push([18, 22, 16, 9]);
    centuriesTable.push([15, 8]);
    centuriesTable.push([17, 21, 14, 7]);
    centuriesTable.push([13, 6]);

    var d = day;
    var m = monthTable[month-1];
    var y;
    for(var i in yearsTable) {
        if(yearsTable[i].indexOf(year) > -1) {
            y = parseInt(i);
            break;
        }
    }
    var c;
    for(var i in centuriesTable) {
        if(centuriesTable[i].indexOf(century) > -1) {
            c = parseInt(i);
            break;
        }
    }

    var y4 = parseInt(year/4);
    var res = (d+m+year+y4+c)%7;

    return res;
}

// console.log('month');
// console.log(parseInt((2.6*(((m+9)%12)+1) - 0.2)%7));
// console.log('year');
// console.log(parseInt(((y%28) + ((y%28)/4))%7));
// console.log('century');
// console.log(c%4);
// console.log((d + parseInt((2.6*m) - 0.2) + parseInt((y + (y/4))) + (c/4) - (2*c))%7);

// test.forEach(month => {
//     console.log(parseInt((2.6*(((month+9)%12)+1) - 0.2)%7));
//
// });
// console.log('\n')
// console.log('\n')
// console.log('\n')

// test2.forEach(year => {
//    console.log(parseInt((year + (year/4))%7))
// });
// console.log('\n')
// console.log(7%4)

console.log(calcDayOfWeek());