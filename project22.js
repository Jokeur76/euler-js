var start = new Date().getTime();
var result;
var unit = "ms";
fs = require('fs')
//code here
var datas
fs.readFile('./docs/p022_names.txt', 'utf8', function (err,data) {
    if (err) {
        return console.log(err);
    }
    data = data.replace(/\"/g, '');
    datas = data.split(',');
    datas.sort();
    // console.log(datas);
    var alphabetical = ['','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
    var count = 1, score;
    result = 0;
    datas.forEach(name => {
        score = 0;
        for(var i = 0; i < name.length; i++) {
            score += alphabetical.indexOf(name[i]);
        }

        result += (score*count);
        count++;
    });

    var duration = new Date().getTime() - start;
    if(duration > 60000) {
        duration = (duration/1000)/60;
        unit = "min";
        console.log("Algo a revoir :/")
    } else if(duration > 60) {
        unit = "sec";
        duration = duration/1000
    }
    console.log("resultat : " + result + " | Duration : " + duration + " " + unit);
});
