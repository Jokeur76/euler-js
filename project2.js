var start = new Date().getTime();
var result = 2;
var fiboArray = [1,2];
var newEntry = 0;
var continu = true;

while(continu) {
    newEntry = fiboArray[0] + fiboArray[1];
    if(newEntry > 4000000) {
        continu = false;
    } else {
        fiboArray[0] = fiboArray[1];
        fiboArray[1] = newEntry;
        if(newEntry%2 == 0) result += newEntry;
    }
}
var duration = new Date().getTime() - start;
console.log("resultat : " + result + ". Duration : " + duration);