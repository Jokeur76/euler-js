var start = new Date().getTime();
var result;
var unit = "ms";

//code here
// structure = {nbr: 0, factSum: 0, amicable: false}
var factorSum = [], current = {};
for (var i = 1; i < 10000; i++) {
    current = {};
    current = {nbr: i, factSum: 0, amicable: false};
    for(var j = 1; j < i; j++) {
        var div = i / j;
        //si pas de chiffre après la virgule : facteur
        if(div - parseInt(div) === 0) {
            current.factSum += j;
            // console.log(div);
        }
    }
    factorSum.push(current);
    // console.log(current);
}
result = 0;
factorSum.forEach(fact => {
    if(!fact.amicable) {
        factorSum.forEach(amicable => {
            if(fact.factSum === amicable.nbr && fact.nbr === amicable.factSum && fact.nbr !== amicable.nbr) {
                fact.amicable = true;
                amicable.amicable = true;
                result += fact.nbr;
                result += amicable.nbr;
                // console.log('amicable found : ' + fact.nbr + ' and ' + amicable.nbr);
            }
        })
    }
});

var duration = new Date().getTime() - start;
if(duration > 60000) {
    duration = (duration/1000)/60;
    unit = "min";
    console.log("Algo a revoir :/")
} else if(duration > 60) {
    unit = "sec";
    duration = duration/1000
}
console.log("resultat : " + result + " | Duration : " + duration + " " + unit);
