var start = new Date().getTime();
var result;
var a = 1;
var b = 2;
var c = 997;
var x;
var isNewTour = true;
while (c > 335) {
    x = 997 - c;
    if(isNewTour) {
        b = 2 + x;
        a = 1;
        isNewTour = false;
    }
    if(a+b+c == 1000 && Math.pow(a, 2) + Math.pow(b, 2) == Math.pow(c, 2)) {
        result = a*b*c;
        break;
    } else {
        if(b-1 <= a+1) {
            c--;
            isNewTour = true;
        } else {
            b--;
            a++;
        }
    }
}

var duration = new Date().getTime() - start;
if(duration >= 60000) {
    console.log("resultat non trouvé avant le temps temps imparti : " + duration + "ms");
} else {
    console.log("resultat : " + result + ". Duration : " + duration + " ms");
}
