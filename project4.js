var start = new Date().getTime();
var result = 0;
var data;
var stringData;
var stringDataReverse;
for(var i = 999; i > 100; i--) {
    for(var j = 999; j > 100; j--) {
        data = i*j;
        //console.log("i : " + i + ", j : " + j);
        //console.log("data : " + stringData + ", data reverse : " + stringDataReverse);
        if(isPalindrome(data) && data > result) {
            result = data;
        }
    }

}
function isPalindrome(data) {
    stringData = data.toString();
    stringDataReverse = stringData.split("").reverse().join("");
    //console.log("data : " + stringData + ", data reverse : " + stringDataReverse);

    return stringData == stringDataReverse;
}

var duration = new Date().getTime() - start;
console.log("resultat : " + result + ". Duration : " + duration);