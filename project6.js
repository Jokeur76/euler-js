var start = new Date().getTime();
var result;
var sumSquare = 0;
var squareSum = 0;

for(var i = 1; i <= 100; i++) {
    sumSquare += Math.pow(i, 2);
    squareSum += i;
}
squareSum = Math.pow(squareSum, 2);

result = squareSum - sumSquare;

var duration = new Date().getTime() - start;
console.log("resultat : " + result + ". Duration : " + duration);